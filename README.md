# SpeedPyCom – The Django Framework for SaaS

By [https://appliku.com/]()

## Introduction

SpeedPyCom is the collection of settings and requirements that allows you quick start of new production and deployment
ready Django Project for SaaS apps.

## Some reasons you might want to use SpeedPyCom

- Based on Django-Configurations, so while there is very useful base settings, it is easy to extend/override
- Storages ready for S3 or local development
- Credentials and some behaviour controlled via env vars
- other useful apps and tools included

## Discussions:

Join our Discord Community: https://appliku.com/discord

